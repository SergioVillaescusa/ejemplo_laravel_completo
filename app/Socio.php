<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio extends Model
{
	protected $table='socios';
    protected $fillable = [
    'nombre',
    'apellidos',
    'dni',
    'telefono',
    'fecha_de_nacimiento',
    'localidad',
    'provincia',
    'direccion',
    'pais',
    'codigo_postal',
    'ruta',
	'observaciones'
  ];
}