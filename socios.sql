-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3307
-- Tiempo de generación: 25-02-2019 a las 08:54:19
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel_2019`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(10) UNSIGNED NOT NULL,
  `dni` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_de_nacimiento` date NOT NULL,
  `localidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Almansa',
  `provincia` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Albacete',
  `ruta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '02640',
  `direccion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'España',
  `baja` tinyint(1) DEFAULT NULL,
  `observaciones` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `dni`, `nombre`, `apellidos`, `telefono`, `fecha_de_nacimiento`, `localidad`, `provincia`, `ruta`, `codigo_postal`, `direccion`, `pais`, `baja`, `observaciones`, `created_at`, `updated_at`) VALUES
(25, 'aa', 'aa', 'a', 'a', '2019-02-22', 'a', 'a', 'img_5c6ff1190c3b00.09692809_2.jpg', 'a', 'a', 'a', NULL, 'a', '2019-02-22 11:33:55', '2019-02-22 11:54:49'),
(30, 'x', 'x', 'x', 'x', '2019-02-07', 'x', 'x', NULL, 's', 's', 's', NULL, 'srf', '2019-02-25 06:39:00', '2019-02-25 06:52:11'),
(31, 'f', 'f', 'f', 'f', '2019-02-15', 'f', 'f', NULL, 'f', 'f', 'f', NULL, 'f', '2019-02-25 06:51:44', '2019-02-25 06:51:44');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
