<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <title></title>
</head>
<body>
  <nav>
    <div class="nav-wrapper">

    <a class="brand-logo right" href="{{ route('logout') }}">
      Logout
    </a>
    <ul id="nav-mobile" class="left hide-on-med-and-down">
      <li><a href="#">Cofradia</a><li></li>
      <li>
        <a class="" href='{{ route("socios.index") }}'>Socios <span class="sr-only">(current)</span></a>
      </li>
      <li>
        <a class="" href='{{ route("inventario.index") }}'>Inventario</a>
      </li>
        <li>
            <a class="" href='{{ route("cuotas.index") }}'>Cuotas</a>
        </li>
        <li>
            <a class="" href='#'>Prestamos</a>
        </li>
        <li>
            <a class="" href='{{ route("tesoreria.index") }}'>Tesoreria</a>
        </li>
    </ul>
    </div>
  </nav>


  <div class="container">
    @yield('content')
  </div>
    <div>
  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

</body>
</html>