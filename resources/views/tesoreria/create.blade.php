@extends('layouts.layout')

	@section('content')
@if ($errors->any())
	<div>
		@foreach($errors->all() as $error)
		{{ $error }}
		@endforeach
	</div>
@endif
<form method="post" action="{{ route('tesoreria.store') }}" enctype="multipart/form-data">
	@csrf
	<div class="form-group col-4 offset-md-4">
	<label>Nombre</label>
    <input type="text" name="nombre" id="nombre" value="" class="form-control">
	<label>Telefono</label>
	<input type="text" class="form-control" name="telefono" id="telefono" value="">
	<label>Fecha</label>
	<input type="date" class="form-control" name="fecha" id="fecha" value="">
	<label>Descripcion</label>
	<input type="text" class="form-control" name="description" id="description" value="">
	<label>Precio</label>
	<div class="input-group mb-3">
  	<div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">€</span>
  	</div>
	<input type="text" class="form-control" name="precio" id="precio" value="">
	</div>
	<label for="imagen">Imagen:</label>
	<input type="file" class="form-control-file" name="imagen" id="imagen" >
	<br>
	</div>
	<button type="submit" class="col-2 offset-md-10">Guardar</button>
</form>
<a class="btn btn-success" href="{{ route('tesoreria.index') }}">Volver</a>
@endsection