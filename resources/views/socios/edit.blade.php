<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

</head>
<body>
 @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
@extends('layout.layoutedit')
@section('content')
	<form action="{{ route('socios.update', $socio->id) }}" method="post" enctype="multipart/form-data">
		@method('PUT')
		@csrf
	  <div class="row">
	    <div class="input-field col s6">
	      <label for="Nombre">Nombre</label>
	      <input type="text" class="validate" id="nombre" name="nombre" value="<?php echo $socio->nombre ?>">
	    </div>
	    <div class="input-field col s6">
	      <label for="Apellidos">Apellidos</label>
	      <input type="text" class="validate" id="apellidos" name="apellidos" value="<?php echo $socio->apellidos ?>">
	    </div>
	  </div>
		<div class="row">
	  <div class="input-field col s12">
	    <label for="inputAddress">DNI</label>
	    <input type="text" class="validate" id="dni" name="dni" value="<?php echo $socio->dni ?>">
	  </div>
		</div>
	<div class="row">
	  <div class="input-field col s12">
	    <label for="telefono">Telefono</label>
	    <input type="text" class="validate" id="telefono" name="telefono" value="<?php echo $socio->telefono ?>">
	  </div>
	</div>
	  <div class="row">
	    <div class="col s6">
	      <label for="fecha">Fecha de nacimiento</label>
	      <input type="date" class="datepicker" id="fecha" name="fecha" value="<?php echo $socio->fecha_de_nacimiento ?>">
	    </div>
	    <div class="input-field col s3">
	      <label for="inputState">Localidad</label>
	      <input type="text" class="validate" id="localidad" name="localidad" value="<?php echo $socio->localidad ?>">
	    </div>
	    <div class="input-field col s3">
	      <label for="provincia">Provincia</label>
	      <input type="text" class="validate" id="provincia" name="provincia" value="<?php echo $socio->provincia ?>">
	    </div>
	  </div>
	  <div class="row">
		  <div class="input-field col s12">
		    <label for="direccion">Direccion</label>
		    <input type="text" class="validate" id="direccion" name="direccion" value="<?php echo $socio->direccion ?>">
		  </div>
	  </div>	  
	  <div class="row">
		  <div class="input-field col s6">
		    <label for="pais">Pais</label>
		    <input type="text" class="validate" id="pais" name="pais" value="<?php echo $socio->pais ?>">
		  </div>
		  <div class="input-field col s6">
		    <label for="codigo">Codigo Postal</label>
		    <input type="text" data-length="10" class="validate" id="codigop" name="codigop" value="<?php echo $socio->codigo_postal ?>">
		  </div>
	  </div>	
	  <div class="row">
		  <div class="input-field col s12">
	    <label for="observaciones">Observaciones</label>
	    <textarea class="materialize-textarea" data-length="255" id="observaciones" name="observaciones" rows="2"><?php echo $socio->observaciones ?></textarea>
		  </div>
	  </div>
		<div class="row">
            <?php if (!empty($socio->ruta)){ ?>
				<label for="imagen">Imagen</label>
				<img src="{{ asset ('storage/'.$socio->ruta) }}">
				<br>
				<input type="file" name="fichero" id="fichero">
            <?php }
            else { ?>
			<div class="row">
				<div class="form-group col-md-12">
					<input type="file" name="fichero" id="fichero">
				</div>
			</div>
            <?php } ?>
		</div>
	  <button type="submit" class="waves-effect waves-light btn-small">Actualizar</button>
	  <a type="button" class="btn waves-effect waves-light" href='{{ route("socios.index") }}'> Volver</a>
	</form>
@endsection


 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>